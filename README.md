# mysql-pg-demo
# 说明
# 初始化数据库
1. 在 mysql 数据库中执行 mysql_user_info.sql 初始化数据库。
2. 在 postgresql 数据库中 执行 postgresql_public_user_info.sql 脚本。
# 配置连接
在 config 文件夹下完成对application.yml 文件中的数据库配置。

# 更多详情
请关注博客 https://blog.csdn.net/senver_wen
