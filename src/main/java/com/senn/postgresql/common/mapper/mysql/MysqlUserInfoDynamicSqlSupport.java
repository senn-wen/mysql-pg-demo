package com.senn.postgresql.common.mapper.mysql;

import java.sql.JDBCType;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class MysqlUserInfoDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: user_info")
    public static final MysqlUserInfo mysqlUserInfo = new MysqlUserInfo();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: user_info.user_id")
    public static final SqlColumn<Integer> userId = mysqlUserInfo.userId;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: user_info.user_age")
    public static final SqlColumn<Integer> userAge = mysqlUserInfo.userAge;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: user_info.user_address")
    public static final SqlColumn<String> userAddress = mysqlUserInfo.userAddress;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: user_info")
    public static final class MysqlUserInfo extends SqlTable {
        public final SqlColumn<Integer> userId = column("user_id", JDBCType.INTEGER);

        public final SqlColumn<Integer> userAge = column("user_age", JDBCType.INTEGER);

        public final SqlColumn<String> userAddress = column("user_address", JDBCType.VARCHAR);

        public MysqlUserInfo() {
            super("user_info");
        }
    }
}