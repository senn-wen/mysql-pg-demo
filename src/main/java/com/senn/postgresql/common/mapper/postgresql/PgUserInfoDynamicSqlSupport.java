package com.senn.postgresql.common.mapper.postgresql;

import java.sql.JDBCType;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class PgUserInfoDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: user_info")
    public static final PgUserInfo pgUserInfo = new PgUserInfo();

    /**
     * Database Column Remarks:
     *   用户id
     */
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: user_info.user_id")
    public static final SqlColumn<Integer> userId = pgUserInfo.userId;

    /**
     * Database Column Remarks:
     *   用户年龄
     */
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: user_info.user_age")
    public static final SqlColumn<Integer> userAge = pgUserInfo.userAge;

    /**
     * Database Column Remarks:
     *   用户地址
     */
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source field: user_info.user_address")
    public static final SqlColumn<String> userAddress = pgUserInfo.userAddress;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", comments="Source Table: user_info")
    public static final class PgUserInfo extends SqlTable {
        public final SqlColumn<Integer> userId = column("user_id", JDBCType.INTEGER);

        public final SqlColumn<Integer> userAge = column("user_age", JDBCType.INTEGER);

        public final SqlColumn<String> userAddress = column("user_address", JDBCType.VARCHAR);

        public PgUserInfo() {
            super("user_info");
        }
    }
}