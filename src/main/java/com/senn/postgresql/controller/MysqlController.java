package com.senn.postgresql.controller;

import com.senn.postgresql.common.mapper.mysql.MysqlUserInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;
import static com.senn.postgresql.common.mapper.mysql.MysqlUserInfoDynamicSqlSupport.*;
import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;

/**
 * @author zhang_wg
 * @version 1.0.0
 * @ClassName MysqlController.java
 * @Description T
 * @createTime 2021年01月15日 17:46:00
 */
@RestController
@Slf4j
public class MysqlController {
    @Resource
    private MysqlUserInfoMapper mysqlUserInfoMapper;
    @GetMapping(path = "/mysql/userInfo")
    public void getUserInfo(){
        List userInfo = mysqlUserInfoMapper.select(c->c.where(userId,isEqualTo(1)));
        log.info(userInfo.toString());
    }

}
