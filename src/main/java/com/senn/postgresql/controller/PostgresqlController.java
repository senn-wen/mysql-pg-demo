package com.senn.postgresql.controller;

import com.senn.postgresql.common.mapper.mysql.MysqlUserInfoMapper;
import com.senn.postgresql.common.mapper.postgresql.PgUserInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static com.senn.postgresql.common.mapper.mysql.MysqlUserInfoDynamicSqlSupport.userId;
import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;

/**
 * @author zhang_wg
 * @version 1.0.0
 * @ClassName PostgresqlController.java
 * @Description postgresql 查询测试
 * @createTime 2021年01月17日 14:56:00
 */
@Slf4j
@RestController
public class PostgresqlController {
    @Resource
    private PgUserInfoMapper pgUserInfoMapper;
    @GetMapping(path = "/pg/userInfo")
    public void getUserInfo(){
        List userInfo = pgUserInfoMapper.select(c->c.where(userId,isEqualTo(0)));
        log.info(userInfo.toString());
    }
}
