create table user_info
(
    user_id      int          not null
        primary key,
    user_age     int          null,
    user_address varchar(255) null
)
    comment '用户信息';

INSERT INTO user_info (user_id, user_age, user_address) VALUES (1, 1, 'mysql');