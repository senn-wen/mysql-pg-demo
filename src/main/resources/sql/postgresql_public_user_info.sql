create table user_info
(
    user_id      integer,
    user_age     integer,
    user_address varchar
);

comment on table user_info is '用户表';

comment on column user_info.user_id is '用户id';

comment on column user_info.user_age is '用户年龄';

comment on column user_info.user_address is '用户地址';

alter table user_info
    owner to "postGIS";

INSERT INTO public.user_info (user_id, user_age, user_address) VALUES (0, 0, 'postgersql');